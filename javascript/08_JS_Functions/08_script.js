/*
  1) Simple Functions ,
  2) args functions ,
  3) return values ,
  4) function expressions ,
  5) functions as arguments
  6) functions inside an object
  7) Nested Objects with functions ,
  8) default args
*/

// let a = 12;
// let b = 20;

// function add() {
//     return a + b;
// }

// console.log(add());

// function add(a, b) {
//     console.log('this is return example');
//     return a + b;
// }

// console.log(add(12, 20));
// let funA = () => {
//     let obj1 = {
//         '1st_name': 'John',
//         '2nd_name': 'Doe',
//         'age': 23,
//         'address': [{
//                 'title': 'addr1',
//                 'city': 'bangalore',
//                 'pincode': '890789',
//                 'state': 'KA'
//             },
//             {
//                 'title': 'addr2',
//                 'city': 'bangalore',
//                 'pincode': '890789',
//                 'state': 'KA'
//             },
//             {
//                 'title': 'addr3',
//                 'city': 'bangalore',
//                 'pincode': '890789',
//                 'state': 'KA'
//             },
//             {
//                 'title': 'addr4',
//                 'city': 'bangalore',
//                 'pincode': '890789',
//                 'state': 'KA'
//             }
//         ]
//     };
//     //
//     return obj1;
// }

// console.log(funA());

// function multiple(a, b = 3) {
//     return `multiplication of ${a} and ${b} is ${a * b}`
// }

function multiple(a, b = 3) {
    return a * b;
}

console.log(`multiplication of a and b is ${multiple(23, 4)}`);
console.log(`multiplication of a and b is ${multiple(10)}`); // till this