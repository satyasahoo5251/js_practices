$(document).ready(function () {
    let username = $('#username');
    let password = $('#password');
    let email = $('#email');
    let submitBtn = $('#submit-button');
    
    submitBtn.click(() => {
        let user = {
            username: username.val(),
            password: password.val(),
            email: email.val()
        };
        let userDetails = {
            username: $('#user_text'),
            password: $('#pass_text'),
            email:  $('#email_text')
        };
        console.log(user)
        userDetails.username.text(user.username);
        userDetails.password.text(user.password);
        userDetails.email.text(user.email);
    })
});