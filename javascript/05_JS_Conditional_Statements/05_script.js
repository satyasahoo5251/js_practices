// If Else condition Example

let a = 7;
let b = 8;

if (a === b) {
    // console.log(a);
} else {
    // console.log(b);
}

// For loop Example to display from 1 - 10 values

// for (let i = 1; i <= b; i++) {
//     for (let j = 1; j <= i; j++) {
//         console.log(i, j);
//     }
// }


// While loop Example to display from 1 - 10 values
// let i = 100;
// while (i < 20) {
//     let x = i;
//     console.log(x);
//     i++;
// }

// Do while loop Example to display from 1 - 10 values

// do {
//     let x = i;
//     console.log(x);
//     i++;
// } while (i > (30 - 20 * 2))
// Switch Statement Example