let speak = new SpeechSynthesisUtterance();

let add = (a, b) => {
    speak.text = a + b;
    window.speechSynthesis.speak(speak);
    console.log(a + b);
    return a + b;
}

let minus = (a, b) => {
    speak.text = a - b;
    window.speechSynthesis.speak(speak);
    console.log(a - b);
    return a - b;
}

let modular = (a, b) => {
    speak.text = a % b;
    window.speechSynthesis.speak(speak);
    console.log(a % b);
    return a % b;
}

let multi = (a, b) => {
    speak.text = a * b;
    window.speechSynthesis.speak(speak);
    console.log(a * b);
    return a * b;
}

let operaton = (operator, a, b) => {
    switch (operator) {
        case '+':
            add(a,b);
            break;
        case '-':
            minus(a,b);
            break;
        case '%':
            modular(a,b);
            break;
        case '*':
            multi(a,b);
            break;

        default:
            alert('Please choose atleast one operator!')
            break;
    }
}

operaton('+', 30, 50)