// document.getElementById(id).onclick = function(){code}	Adding event handler code to an onclick event
// document.getElementById('button').onclick = () => {
//     // alert('Clicked the button');
//     document.getElementById('myCard').style.display = 'inline-block';
// }

// document.getElementById('button1').onclick = () => {
//     document.getElementById('myCard').style.display = 'none';
// }

// task1 = use two bulb image(off, on)

document.getElementById('myCard').style.border = '2px solid green';
document.getElementById('myCard').style.backgroundColor = 'lightgray';
document.getElementById('myCard').style.borderRadius = '5px';
document.getElementsByClassName('card-title')[0].style.color = '#0073aa';
// document.getElementsByClassName('card-title')[0].style.backgroundColor = '#e5d7d7';
document.getElementsByClassName('card-title')[0].style.border = '2px solid lightgreen';
// document.getElementsByClassName('card-title')[0].style.borderWidth = '10px';
// document.getElementsByClassName('card-title')[0].style.width = '25%';
document.getElementsByClassName('card-text')[0].style.color = '#0073aa';
