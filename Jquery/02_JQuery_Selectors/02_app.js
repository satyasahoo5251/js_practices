/*
    1) Tag Selector
    2) Class Selector
    3) Id Selector
    4) Attribute Selector

 */

    $('p').text('Helo World')
    $('.paragraph').text('helo World')
    $('#paragraph').text('helo World')
    $('p[id=paragraph]').text('helo world2');
    $('p[class=paragraph]').text('helo world1');