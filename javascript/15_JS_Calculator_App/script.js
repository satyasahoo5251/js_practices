let num1 = document.getElementById('firstNumber');
let num2 = document.getElementById('secondNumber');
let result = document.getElementById('result');

//operators
let parentOpp = document.getElementById('parentOpp');
let plusOperator = document.getElementById('plus-button');
let minusOperator =  document.getElementById('minus-button');
let divideOperator =  document.getElementById('divide-button');
let multiplyOperator =  document.getElementById('multiply-button');
let clearOperator =  document.getElementById('clear-button');
let operator = document.getElementById('operator');


let equals = document.getElementById('equals-button');

equals.onclick = () => {
    // console.log(num1.value, num2.value);
    switch (operator.textContent) {
        case '+':
           result.value =  Number(num1.value) + Number(num2.value)
            break;

        case '-':
            result.value =  Number(num1.value) - Number(num2.value)
             break;
    
        case '/':
           result.value =  Number(num1.value) / Number(num2.value)
            break;

        case '*':
            result.value =  Number(num1.value) * Number(num2.value)
             break;

        default:
            alert('Please select some operator!')
            break;
    }
}

plusOperator.onclick = () => {
    operator.textContent = '+';
}
minusOperator.onclick = () => {
    operator.textContent = '-';
}
divideOperator.onclick = () => {
    operator.textContent = '/';
}
multiplyOperator.onclick = () => {
    operator.textContent = '*';
}
clearOperator.onclick = () => {
    operator.textContent = '+';
    num1.value = 0;
    num2.value = 0;
    result.value = 0;
}