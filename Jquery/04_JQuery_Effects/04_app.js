/*  USE BS Cards
    1) Show + Hide => Toggle
    2) FadeIn + FadeOut => FadeToggle
    3) slideUp + SlideDown => SlideToggle
    4) animate
    5) Stop
 */

    // function add() {
    //     //state
    // }

// let btnEl = $('#btnJq');
// let cardEl = $('.card')

// console.log(cardEl);

// btnEl.click(() => {
//     cardEl.slideToggle();
// })

$(document).ready(() => {
    let btnEl = $('#btnJq');
    let btnElReset = $('#btnReset');
    let cardEl = $('.card');

    // btnEl.click(() => {
    //     cardEl.animate({height: '18rem'})
    // })
    // btnElReset.click(() => {
    //     cardEl.animate({height: '13rem'})
    // })

    btnEl.click(() => {
        cardEl.slideDown(5000)
    })
    btnElReset.click(() => {
        cardEl.stop()
    })

})