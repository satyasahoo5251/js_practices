let username = document.getElementById('username');
let email = document.getElementById('email');
let password = document.getElementById('password');
let cnfPassword = document.getElementById('password_confirm');

let validate = () => {
    // alert('clicked resister button')
    let user = {
        usr_name: username.value,
        email: email.value,
        password: password.value,
        cnfPassword: cnfPassword.value
    }

    if(user.usr_name === null || user.usr_name === '') {
        // alert("Username shouldn't be empty!")
        document.getElementById('username').style.border = '1px solid red';
        document.getElementById('name_error').innerText = "User name shouldn't be empty";
        document.getElementById('name_error').style.color = "red";
        document.getElementById('name_error').style.textAlign = "center";
    } else {
        document.getElementById('username').style.border = '1px solid black';
        document.getElementById('name_error').innerText = "";
    }

    if(user.email === null || user.email === '') {
        // alert("email shouldn't be empty!")
        document.getElementById('email').style.border = '1px solid red';
        document.getElementById('email_error').innerText = "Email shouldn't be empty";
        document.getElementById('email_error').style.color = "red";
        document.getElementById('email_error').style.textAlign = "center";
    } else {
        document.getElementById('email').style.border = '1px solid black';
        document.getElementById('email_error').innerText = "";
    }

    if(user.password === null || user.password === '') {
        // alert("password shouldn't be empty!")
        document.getElementById('password').style.border = '1px solid red';
        document.getElementById('pass_error').innerText = "Password shouldn't be empty";
        document.getElementById('pass_error').style.color = "red";
        document.getElementById('pass_error').style.textAlign = "center";
    } else {
        document.getElementById('password').style.border = '1px solid black';
        document.getElementById('pass_error').innerText = "";
    }

    if(user.cnfPassword === null || user.cnfPassword === '') {
        // alert("cnfPassword shouldn't be empty!")
        document.getElementById('password_confirm').style.border = '1px solid red';
        document.getElementById('password_error').innerText = "Cnf Password shouldn't be empty";
        document.getElementById('password_error').style.color = "red";
        document.getElementById('password_error').style.textAlign = "center";
    } else {
        document.getElementById('password_confirm').style.border = '1px solid black';
        document.getElementById('password_error').innerText = "";
    }

    

    if(user.usr_name !== '' && user.email !== '' && user.password !== ''
        && user.cnfPassword !== '') {
            console.log(user);
            document.getElementById('username').style.border = '1px solid black';
            document.getElementById('email').style.border = '1px solid black';
            document.getElementById('password').style.border = '1px solid black';
            document.getElementById('password_confirm').style.border = '1px solid black';
            localStorage.setItem('user', JSON.stringify(user));
            let result = JSON.parse(localStorage.getItem('user'));
            document.getElementById('user_name').textContent = result.usr_name;
            document.getElementById('e_mail').textContent = result.email;
            document.getElementById('pass').textContent = result.password;
        }
        
}