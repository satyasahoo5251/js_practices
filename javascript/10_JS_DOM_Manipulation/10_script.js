// JavaScript can change all the HTML elements in the page
// JavaScript can change all the HTML attributes in the page
// JavaScript can change all the CSS styles in the page
// JavaScript can remove existing HTML elements and attributes
// JavaScript can add new HTML elements and attributes
// JavaScript can react to all existing HTML events in the page
// JavaScript can create new HTML events in the page


// document.getElementById(id)	Find an element by element id
// document.getElementsByTagName(name)	Find elements by tag name
// document.getElementsByClassName(name) Find elements by class name


// document.createElement(element)	Create an HTML element
// document.removeChild(element)	Remove an HTML element
// document.appendChild(element)	Add an HTML element
// document.replaceChild(new, old)	Replace an HTML element
// document.write(text)	Write into the HTML output stream

let a = 10;
let b = 20;

// let myId = 'helo';
// document.getElementsByClassName('world')[0].textContent = 12;
// document.getElementsByClassName('world')[1].textContent = 34;
// document.getElementsByClassName('world')[2].textContent = 2;
// document.getElementsByClassName('world')[3].textContent = 0;
// document.getElementById('helo').innerHTML = `Sum of ${a} & ${b} is ${a + b}`;
// for (let i = 0; i <= 3; i++) {
//     document.getElementsByClassName('world')[i].textContent = 'Helo';
// }
// document.getElementsByClassName('world').innerHTML = 'Helo'
// console.log(document.getElementsByClassName('.world').innerHTML = 'Helo');
// document.getElementsByTagName('p')[3].textContent = 'Helo';

let ai = document.createElement('p');

// ai.innerText = 'Helo World';
// al.innerText = 'World';
// listITm.replaceChild(ai, listITm.childNodes[0])


document.body.appendChild(ai)

let el1 = document.createTextNode('Element 1')

let listITm = document.getElementById('listItm').children[2];

ai.appendChild(el1);
listITm.replaceChild(el1, listITm.childNodes[0])
    // ai.removeChild(ai.childNodes[0])

// al.replaceChild(ai, al)