/* ------------------------------------------------------------------
                            Math Object Examples
 -------------------------------------------------------------------- */

// Math PI

// Math sqrt

// find the min of 4 numbers

// find the Max of 4 numbers

// find the 'x' to the power of 'y' value, ex: 2^4

// generate the random numbers from 0 to 100


/* For more details about Math Object, please have a look at
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math
*/

/* ------------------------------------------------------------------
                            Date Object Examples
 -------------------------------------------------------------------- */

// get today's date

// Get proper date

// get full day of the week using switch statement

// Display a Digital Clock on the web page


/* For More Details about Date() please have a look at
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
*/

/* ------------------------------------------------------------------
                            Number Object Examples
 -------------------------------------------------------------------- */



/* ------------------------------------------------------------------
                            String Object Examples
 -------------------------------------------------------------------- */

// console.log('returns Euler number ' + Math.E); // returns Euler's number
// console.log('Math Pi value ' + Math.PI); // Math.PI     // returns PI
// Math.SQRT2    // returns the square root of 2
// Math.SQRT1_2  // returns the square root of 1/2
// Math.LN2      // returns the natural logarithm of 2
// Math.LN10     // returns the natural logarithm of 10
// Math.LOG2E    // returns base 2 logarithm of E
// Math.LOG10E

// Math.round()

// console.log(Math.round(4.5));
// Math.round(4.5);
// Math.round(4.4);

// Math.ceil()
// console.log(Math.ceil(4.49));
// Math.ceil(4.7);
// Math.ceil(4.4);
// Math.ceil(4.2);
// Math.ceil(-4.2);

// Math.floor()
// console.log(Math.floor(4));
// Math.floor(4.7);
// Math.floor(4.4);
// Math.floor(4.2);
// Math.floor(-4.2);

// Math.trunc()
// Math.trunc(4.9);
// Math.trunc(4.7);
// Math.trunc(4.4);
// Math.trunc(4.2);
// Math.trunc(-4.2);

// Math.sign()
// Math.sign(-4);
// Math.sign(0);
// Math.sign(4);

// Math.pow();
let getPower = (a, b) => {
        return console.log(Math.pow(a, b));
    }
    // console.log(Math.pow(3, 6));

getPower(3, 5)

// Math.sqrt()
// console.log(Math.sqrt(64));

// Math.abs()
// Math.abs(-4.7);


// Math.sin()

// Math.min() and Math.max()

// console.log(Math.min(0, 150, 30, 20, -8, -200));
// console.log(Math.max(0, 150, 30, 20, -8, -200));

// Math.random()
// Math.random();