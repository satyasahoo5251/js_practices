// creation of arrays

let array = [{
    "id": 1,
    "first_name": "Waneta",
    "last_name": "Smidmor",
    "email": "wsmidmor0@google.pl",
    "gender": "Non-binary",
    "ip_address": "54.124.50.59"
}, {
    "id": 2,
    "first_name": "Archer",
    "last_name": "Lilleman",
    "email": "alilleman1@jigsy.com",
    "gender": "Polygender",
    "ip_address": "86.196.69.219"
}, {
    "id": 3,
    "first_name": "Zolly",
    "last_name": "Ruffli",
    "email": "zruffli2@google.co.jp",
    "gender": "Male",
    "ip_address": "60.65.66.165"
}, {
    "id": 4,
    "first_name": "Masha",
    "last_name": "Lamberth",
    "email": "mlamberth3@t-online.de",
    "gender": "Polygender",
    "ip_address": "27.9.16.110"
}, {
    "id": 5,
    "first_name": "Andree",
    "last_name": "Adami",
    "email": "aadami4@sphinn.com",
    "gender": "Male",
    "ip_address": "167.159.218.138"
}, {
    "id": 6,
    "first_name": "Hertha",
    "last_name": "Doyle",
    "email": "hdoyle5@whitehouse.gov",
    "gender": "Polygender",
    "ip_address": "223.122.93.240"
}, {
    "id": 7,
    "first_name": "Laetitia",
    "last_name": "Toffaloni",
    "email": "ltoffaloni6@angelfire.com",
    "gender": "Genderqueer",
    "ip_address": "126.196.249.209"
}, {
    "id": 8,
    "first_name": "Anastassia",
    "last_name": "Verring",
    "email": "averring7@reddit.com",
    "gender": "Male",
    "ip_address": "229.211.242.47"
}, {
    "id": 9,
    "first_name": "Urbanus",
    "last_name": "Lilley",
    "email": "ulilley8@ucoz.ru",
    "gender": "Agender",
    "ip_address": "92.189.111.84"
}, {
    "id": 10,
    "first_name": "Geoffrey",
    "last_name": "Cornthwaite",
    "email": "gcornthwaite9@vk.com",
    "gender": "Female",
    "ip_address": "202.151.40.73"
}];

let array1 = ['a', 'b', 'c', 'd'];
array1.pop();
array1.push('e')
console.log(array1);
// console.log(array);

// for (let i = 0; i < array.length; i++) {
//     console.log(array[i].email);
// }

// array.forEach((el) => {
//    console.log(el.first_name); 
// })

// array.map((el) => {
//     if (el.id === 5) {
//         console.log(el.email);
//     }
//     // Object.assign(el, { dob: '08/04/2021' })
// })

// Accessing an array and its properties

// console.log(array[3].id);

// Access not existed property from an array

// console.log(array[0].age);

// console.log(array);
// adding properties to an array

// array.push({ dob: '09/08/2021' })

// console.log(array);


// Accessing length of an Array //// till it



// reverse the array using reverse()


// array.reverse();

// console.log(array);


// Remove the first value of the array: shift()

// array.shift();

// console.log(array);


// Add value to front of the array:unshift()
// array.unshift({
//     "id": 0,
//     "first_name": "Geoffrey",
//     "last_name": "Cornthwaite",
//     "email": "gcornthwaite9@vk.com",
//     "gender": "Female",
//     "ip_address": "202.151.40.73"
// }, {
//     "id": 0,
//     "first_name": "Geoffrey",
//     "last_name": "Cornthwaite",
//     "email": "gcornthwaite9@vk.com",
//     "gender": "Female",
//     "ip_address": "202.151.40.73"
// });

// console.log(array);

// Remove the last value of the array: pop()

// array.pop();

// console.log(array);


// Add value the end of the array: push()

// array.push({
//     "id": 11,
//     "first_name": "Geoffrey",
//     "last_name": "Cornthwaite",
//     "email": "gcornthwaite9@vk.com",
//     "gender": "Female",
//     "ip_address": "202.151.40.73"
// })

// console.log(array);

// Remove an element from an Array , Arguments: colors.splice(pos,n):

// array.splice(3, 6)

// console.log(array);


// Create a copy of an array. Typically assigned to a new variable:slice();

// console.log(array);
// let arrya2 = array.slice(1, 3);

// console.log(arrya2);

// indexOf()
// let frontendTch = ["HTMl", "PHP", "Javascript"]
// console.log(frontendTch.indexOf('PHP'), frontendTch[frontendTch.indexOf('PHP')]);

// join() , split()

// let joinArrayEl = frontendTch.join(" , ");

// console.log(frontendTch.join(" , "));

// console.log(joinArrayEl.split(), frontendTch);


// const array1 = [1, 4, 9, 16];

// pass a function to map
// const map1 = array1.map(x => x * 2);

// console.log(map1);
// expected output: Array [2, 8, 18, 32]


// MDN documentation for Array:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array


// let add = () => {
//     //statements
//     array.map((x) => {
//         if (x.id <= 5) {
//             return console.log(x.first_name + ' ' + x.last_name);;
//         }
//     })

// }

// function add() {
//statements & logics
// array.map((x) => {
//         if (x.id <= 5) {
//             return console.log(x.first_name + ' ' + x.last_name);;
//         }
//     })
// }

// add()


// slice  

// let array2 = array1.slice(0, 3)
// console.log(array2);