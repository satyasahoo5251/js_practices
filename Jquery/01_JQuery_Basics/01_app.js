/*
    1) Inline JQuery with a paragraph
    2) Internal JQuery with a Card
    3) External JQuery with a Card
 */
$(document).ready(() => {
    //statements
    // $('h2').text('Jquery running successfuly')
    $('#paragraph').text('Helo')
    $('#paragraph').append(`<ul> 
    <li>Apple</li><li>Orange</li><li>Mango</li></ul>`)
});