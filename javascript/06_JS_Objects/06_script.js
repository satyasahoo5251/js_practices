/*
 * 1) Object Creation
 * 2) Adding and Deleting props
 * 3) Retrieval
 * 4) Object Literal
 * 5) dot , [] notation
 * 6) Nested Objects
 */

// let user = {};
// let emp = new Object();

let user = {
    first_name: 'John',
    last_name: 'doe',
    age: 29
}

let extra = {dob: '1989/06/09', salary: '12lpa'}

let newUser = Object.assign({}, user, extra)

// delete newUser.dob;

// localStorage.setItem('user', JSON.stringify(newUser))

// console.log(JSON.parse(localStorage.getItem('user')));

// console.log(`newUser firstname is ${newUser['salary']}`);

// console.log(`User first name is ${user_first_name}, last name is ${user_last_name} & age is ${age}.`);

console.log(typeof(user));
